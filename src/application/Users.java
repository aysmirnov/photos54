package application;

import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
/**
 * Users - this class makes a list of all the users in the system.
 * @author Alex Smirnov and Raed Anbarserri
 */
public class Users implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8220095297380463389L;
	private ArrayList<User> list_users;
	private final static String usersFile = "users.ser";
	
	/**
	 * Users constructor - creates a list of all the users in the system.
	 */
	public Users() {

		 if (list_users == null) {
			 list_users = new ArrayList<User>();
		 }
	}
	/**
	 * getUser - gets a specific user from the system.
	 * @param username The username of the user.
	 * @return The user that's being searched for.
	 */
	public User getUser(String username) {
		for (int i = 0; i < list_users.size(); i++) {
			User user = list_users.get(i);
			if (user.getUsername().equals(username))
				return user;
		}
		return null;
	}
	/**
	 * getUsernames - gets the usernames of all the users in the system.
	 * @return The usernames of all the users.
	 */
	public ArrayList<String> getUsernames() {
		ArrayList<User> users = getUsers();
		ArrayList<String> usernames = new ArrayList<String>();
		for (int i = 0; i < users.size(); i++) {
			usernames.add(users.get(i).getUsername());
		}

		return usernames;
	}
	/**
	 * add - adds a user to the list.
	 * @param user The user being added to the list.
	 */
	public void add(User user) {
		add(user.getUsername());
	}
	
	/**
	 * addAlbum - adds an album to a specific user in the list.
	 * @param user The user the album is being added to.
	 * @param album The album being added.
	 */
	public void addAlbum(User user, String album) {
		for (int i = 0; i < list_users.size(); i++) {
			if (list_users.get(i).getUsername().equals(user.getUsername())) {
				list_users.get(i).addAlbum(album);
				return;
			}
		}
	}
	/**
	 * add - Adds a user by username.
	 * @param username The username of the user being added.
	 */
	public void add(String username) {

		// user exists already
		if (findUser(username) != null) {
			return;
		}

		list_users.add(new User(username));
	}
	/**
	 * remove - removes a user by username from the list.
	 * @param username The user being removed.
	 */
	public void remove(String username) {
		for (int i = 0; i < list_users.size(); i++) {
			if (list_users.get(i).getUsername().equals(username)) {
				list_users.remove(i);
				return;
			}
		}
	}
	
	
	//IMPORTANT METHOD THAT STORES EVERYTHING
	/**
	 * store - this method stores all the users in an object.
	 * @param users The users in the list.
	 */
	public static void store(Users users){
		try {
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(usersFile, false));
		oos.writeObject(users);
		oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//IMPORTANT METHOD THAT STORES EVERYTHING
	/**
	 * load - this method loads the users from a list.
	 * @return The loaded users.
	 */
	public static Users load(){
		Users users = null;
		try {
			FileInputStream filestream = new FileInputStream(usersFile);
			ObjectInputStream ois = new ObjectInputStream(filestream);
			Object object = ois.readObject();
			users = (Users) object;
			ois.close();
		} catch (Exception e) {
			System.out.println("Trying to load empty users file...");
			users = new Users();
			store(users);
		}
		return users;
	}
	
	/**
	 * findUser - find a specific user in the list.
	 * @param username The name of a specific user.
	 * @return The user, if found.
	 */
	public User findUser(String username) {
		if (list_users.isEmpty())
			return null;

		for (int i = 0; i < list_users.size(); i++) {
			System.out.println(list_users.get(i).getUsername());
			if (list_users.get(i).getUsername().equals(username)) {
				return list_users.get(i);
			}
		}
		return null;
	}
	
	/**
	 * toString - returns a string of all the users in the list.
	 * @return All of the users in the list.
	 */
	@Override
	public String toString() {
		String str = "";

		for (int i = 0; i < list_users.size(); i++) {
			str += list_users.get(i).getUsername() + "\n";
		}

		return str;

	}
	/**
	 * size - gets the size of the current list.
	 * @return the size of the list.
	 */
	public int size() {
		return list_users.size();
	}
	/**
	 * getUsers - gets the list of users.
	 * @return the list of users.
	 */
	public ArrayList<User> getUsers() {
		return list_users;
	}

}
