package application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
/**
 * Photo - This class stores the details of a picture in the Photos application.
 * @author Alex Smirnov and Raed Anbarserri
 */
public class Photo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1983985260766832706L;
	private String caption;
	private ArrayList<Tag> tags;
	
	//parameters of a digested image
	private int width, height;
	private int[][] pixels;
	
	private Calendar date;
	/**
	 * Photo constructor - creates a Photo object with given image, and gives it details.
	 * @param image The image to use for the Photo object.
	 */
	public Photo(Image image) {

		caption = "";
		tags = new ArrayList<Tag>();
		date = Calendar.getInstance();
		date.set(Calendar.MILLISECOND, 0);
		setImage(image);
		
	}
	/**
	 * returnTags - gets the tags of the photo.
	 * @return the tags of a photo.
	 */
	public ArrayList<Tag> returnTags(){
		return tags;
	}
	
	/**
	 * getTags - gets the tags of the photo.
	 * @return the tags of the photo.
	 */
	public ArrayList<String> getTags(){
		ArrayList<String> string_tags = new ArrayList<String>();
		for (int i = 0; i < tags.size(); i++) {
			string_tags.add(tags.get(i).toString());
		}
		return string_tags;
	}
	/**
	 * getImage - gets the current image.
	 * @return return the current image.
	 */
	public Image getImage() {
		WritableImage image = new WritableImage(width, height);
		
		PixelWriter writer = image.getPixelWriter();
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				writer.setArgb(i, j, pixels[i][j]);
			}
		}
		
		return image;
	}
	/**
	 * setImage - sets the size of the given image.
	 * @param image The image used to pull dimensions from.
	 */
	public void setImage(Image image) {
		width = 0 + (int) image.getWidth();
		height = 0 + (int) image.getHeight();
		pixels = new int[width][height];
		
		PixelReader reader = image.getPixelReader();
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				pixels[i][j] = reader.getArgb(i, j);
			}
		}
	}
	/**
	 * setCaption - sets the current caption of the photo.
	 * @param caption The caption to be added to the photo.
	 */
	public void setCaption(String caption) { this.caption = caption; }
	
	/**
	 * tagExists - checks if a specific tag value pair exists for any photo.
	 * @param name The name of the tag.
	 * @param value The value of the tag.
	 * @return true if tag exists, false if not.
	 */
	public boolean tagExists(String name, String value) {
		for (int i = 0; i < tags.size(); i++) {
			if (tags.get(i).getName().equals(name) && tags.get(i).getValue().equals(value)) return true;
		}
		return false;
	}
	/**
	 * addTag - adds a tag to the given photo.
	 * @param name The tag text.
	 * @param value The placement of the tag.
	 */
	public void addTag(String name, String value) {
		if (tagExists(name,value)) {
			System.out.println("Tag already exists");
			return;
		}
		tags.add(new Tag(name, value));
	}
	/**
	 * removeTag - delete a tag from the given photo.
	 * @param name The tag to be removed.
	 * @param value The placement of the tag.
	 */
	public void removeTag(String name, String value) {
		for (int i = 0; i < tags.size(); i++) {
			if (tags.get(i).getName().equals(name) && tags.get(i).getValue().equals(value))
			tags.remove(i);
		}
	}
	/**
	 * getCaption - get the caption of the photo.
	 * @return the caption of the photo.
	 */
	public String getCaption() {
		return caption;
	}
	/**
	 * getCalendar - returns the calendar date of the photo.
	 * @return the calendar date of the photo.
	 */
	public Calendar getCalendar() {
		return date;
	}
	/**
	 * getDate - returns the time of the photo
	 * @return the time of the photo.
	 */
	public String getDate() {
		return date.getTime().toString();
	}
}
