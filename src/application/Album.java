package application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

import javafx.scene.image.Image;
/**
 * Album - this class stores the details of an album in the system.
 * @author Alex Smirnov and Raed Anbarserri
 */
public class Album implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2099709006680244790L;
	private ArrayList<Photo> photos;
	private String name;
	private int count;
	
	private Photo oldest, newest; //for keeping track of the oldest and newest photos in the album
	/**
	 * Album constructor - creates a new Album object with the proper details.
	 * @param name The name of the new album.
	 */
	public Album(String name) {
		this.name = name;
		photos = new ArrayList<Photo>();
		count = 0;
		oldest = null;
		newest = null;
	}
	/**
	 * findDates - find the oldest and newest dates in the album.
	 */
	public void findDates() {
		findOldest();
		findNewest();
	}
	/**
	 * findOldest - find the oldest photos of the album.
	 */
	public void findOldest() {
		if (photos.size() == 0) {
			oldest = null;
			return;
		}
		
		oldest = photos.get(0);
		Calendar oldest = photos.get(0).getCalendar();
		for (int i = 1 ;i < photos.size(); i++) {
			if (photos.get(i).getCalendar().before(oldest)) {
				oldest = photos.get(i).getCalendar();
				this.oldest = photos.get(i);
			}
		}
	}
	/**
	 * findNewest - find the newest photos in the album.
	 */
	public void findNewest() {
		if (photos.size() == 0) {
			newest = null;
			return;
		}
		
		newest = photos.get(0);
		Calendar newest = photos.get(0).getCalendar();
		for (int i = 1 ;i < photos.size(); i++) {
			if (newest.before(photos.get(i).getCalendar())) {
				newest = photos.get(i).getCalendar();
				this.newest = photos.get(i);
			}
		}
	}
	/**
	 * getPhotos - get the photos in the current album.
	 * @return the photos in the album.
	 */
	public ArrayList<Photo> getPhotos() { return photos; }
	/**
	 * add - add a photo to the current album.
	 * @param image The photo to be added to the new album.
	 */
	public void add(Image image) {
		photos.add(new Photo(image));
	}
	
	/**
	 * add - adds a photo to an album.
	 * @param photo The photo being added to the album.
	 */
	public void add(Photo photo) {
		photos.add(photo);
	}
	/**
	 * remove - remove a photo at a specific index from the current album.
	 * @param index Index of the photo to be removed.
	 */
	public void remove(int index) {
		photos.remove(index);
	}
	/**
	 * remove - remove a specific photo from the current album.
	 * @param photo The photo to be removed.
	 */
	public void remove(Photo photo) {
		photos.remove(photo);
	}
	/**
	 * photoExists - check if a specific photo exists in the album.
	 * @param name The name of the photo.
	 * @return true if exists, false if not.
	 */
	public boolean photoExists(String name) {
		for (int i = 0; i < photos.size(); i++) {
			if (photos.get(i) != null) {
					return true;
			}
		}
		return false;
	}

/*	public void store() {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(photosFile, false));
			oos.writeObject(photos);
			oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}*/
	/**
	 * getThumbnail - gets thumbnails of the photos in the album.
	 * @return thumbnail of the photo.
	 */
	public Image getThumbnail() {
		if (photos.size() == 0) return null;
		else return photos.get(0).getImage();
	}
	/**
	 * getName - gets the name of the album.
	 * @return name of the album.
	 */
	public String getName() { return name; }
	/**
	 * setName - sets name of the album.
	 * @param newName name that album is being changed to.
	 */
	public void setName(String newName) { name = newName; }
	/**
	 * getPhoto - get a photo at a specific index.
	 * @param index Index to pull photo from.
	 * @return the photo at the given index.
	 */
	public Photo getPhoto(int index) {
		return photos.get(index);
	}
	/**
	 * getImage - get the image of a specific photo in the album.
	 * @param index Index of the photo to pull image from.
	 * @return image from the given photo.
	 */
	public Image getImage(int index) {
		return photos.get(index).getImage();
	}
	/**
	 * getSize - gets the size of the album.
	 * @return the size of the album.
	 */
	public int getSize() {
		return photos.size();
	}
	/**
	 * addCount - add 1 to the counter of photos.
	 */
	public void addCount() {
		count++;
	}
	/**
	 * getCount - get the number of photos in the album.
	 * @return number of photos.
	 */
	public int getCount() { return count; }
	/**
	 * getOldestDate - get the oldest date of the album.
	 * @return the oldest date, if it exists.
	 */
	public String getOldestDate() { if (oldest != null) return oldest.getDate(); else return "";}
	/**
	 * getNewestDate - get the newest date of the album.
	 * @return the newest date, if it exists.
	 */
	public String getNewestDate() { if (newest != null) return newest.getDate(); else return ""; }

}
