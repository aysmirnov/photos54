package application;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * User - This class stores all the details of a user in the Photos system.
 * @author Alex Smirnov and Raed Anbarserri
 */
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8484716118729095549L;
	private ArrayList<Album> albums;
	private String username;

	//private String albumFile = "";
	/**
	 * User constructor - creates a new user, and makes an album list for them.
	 * @param username The username of the new user.
	 */
	public User(String username) {
		this.username = username;
		albums = new ArrayList<Album>();
	}
	/**
	 * addAlbum - adds a new album to the user.
	 * @param name The name of the new album.
	 */
	public void addAlbum(String name) {
		if (albumExists(name)) return;
		albums.add(new Album(name));
	}
	/**
	 * addAlbum - adds an existsing album to the user.
	 * @param album The album being added.
	 */
	public void addAlbum(Album album) {
		if (albumExists(album.getName())) return;
		albums.add(album);
	}
	/**
	 * renameAlbum - renames an album that the user has.
	 * @param album The album being renamed.
	 * @param newName The new name of the album.
	 */
	public void renameAlbum(String album, String newName) {
		for (int i = 0; i < albums.size();i++) {
			if (albums.get(i).getName().equals(album)) {
				albums.get(i).setName(newName);
				break;
			}
		}
	}
	/**
	 * deleteAlbum - removes a specific album from the user.
	 * @param album The album being removed.
	 */
	public void deleteAlbum(String album) {
		for (int i = 0; i < albums.size(); i++) {
			if (albums.get(i).getName().equals(album)) {
				albums.remove(i);
			}
		}
	}
	/**
	 * albumExists - checks if a specific album exists in the user's data.
	 * @param name The name of the album.
	 * @return True if exists, false if not.
	 */
	public boolean albumExists(String name) {
		if (albums.isEmpty()) return false;
		
		for (int i = 0; i < albums.size(); i++) {
			if (albums.get(i) != null) {
				if (albums.get(i).getName().equals(name))
					return true;
			}
		}
		return false;
	}
	/**
	 * getUsername - gets the username of the user.
	 * @return The username of the user.
	 */
	public String getUsername() { return username; }
	/**
	 * getAlbums - gets the albums of the user.
	 * @return the albums of the user.
	 */
	public ArrayList<Album> getAlbums() { return albums; }
	
	public ArrayList<String> getAlbumNames() {
		ArrayList<String> arr = new ArrayList<String>();
		for (int i = 0; i < albums.size(); i++) {
			arr.add(albums.get(i).getName());
		}
		return arr;
	}
	/**
	 * getAlbum - get a specific album that the user has.
	 * @param name The name of the album.
	 * @return The album with the given name.
	 */
	public Album getAlbum(String name) {
		for (int i = 0; i < albums.size(); i++) {
			if (albums.get(i).getName().equals(name))
			return albums.get(i);
		}
		return null;
	}

}
