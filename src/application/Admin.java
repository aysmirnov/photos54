package application;

import java.util.ArrayList;
/**
 * Admin - This class holds the details of the system administrator.
 * @author Alex Smirnov and Raed Anbarserri
 */
public class Admin extends User{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5974326611147752188L;
	Users users;
	/**
	 * Admin constructor - creates an admin user.
	 * @param username The username of the admin.
	 */
	public Admin(String username) {
		super(username);
		users = new Users();
	}
	/**
	 * create - Function used to create new users of the system, usable by the admin.
	 * @param username New user username.
	 * @return true if successful, false if not.
	 */
	public boolean create(String username) {
		if (!userExists(username)){
			users.add(new User(username));
			return true;
		}
		
		return false;
	}
	/**
	 * remove - remove a user from the system.
	 * @param username Username of the user to be removed.
	 * @return true if successful, false if not.
	 */
	public boolean remove(String username) {
		users.remove(username);
		return true;
	}
	/**
	 * list - list of all the usernames in the system.
	 * @return the list of usernames.
	 */
	public ArrayList<String> list() {
		return users.getUsernames();
	}
	/**
	 * userExists - lets the admin know if given user exists or not.
	 * @param username User to search for.
	 * @return true if exists, false if not.
	 */
	private boolean userExists(String username) {
		ArrayList<String> usernames = users.getUsernames();
		for (int i = 0; i < usernames.size(); i++) {
			if (usernames.get(i) != null) {
				if (usernames.get(i).equals(username)) return true;
			}
		}
		return false;
	}

}
