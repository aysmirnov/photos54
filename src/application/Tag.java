package application;

import java.io.Serializable;
/**
 * Tag - this class takes care of all tag related needs for Photos.
 * @author Alex Smirnov and Raed Anbarserri
 */
public class Tag implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4282260651979557517L;
	private String name;
	private String value;
	/**
	 * Tag constructor - creates a name value pair for a tag.
	 * @param tag1 The name of the tag.
	 * @param tag2 Value of the tag.
	 */
	public Tag(String tag1, String tag2) {
		name = tag1;
		value = tag2;
	}
	/**
	 * toString - returns the name and value of the tag
	 * @return name and value string.
	 */
	public String toString() { return name+ ","+value; }
	/**
	 * getName - gets the name of the tag.
	 * @return name of the tag.
	 */
	public String getName() { return name; }
	/**
	 * getValue - gets the value of a tag.
	 * @return the value of the tag.
	 */
	public String getValue() { return value; }
}
