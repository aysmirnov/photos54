package application;
/**
 * Photos Application
 * 
 * This application allows storage and management of photos in one or more albums.
 * 
 * @author Alex Smirnov and Raed Anbarserri
 * @date 4/11/2018
 * @version 1.0
 */
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;
public class Photos extends Application {

	Stage primaryStage;
	boolean end;
	
	/**
	 * start - This class initializes the application by showing the login window.
	 * @param primaryStage The primary stage of the application.
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			this.primaryStage = primaryStage;
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("LoginWindow.fxml"));
			BorderPane root = (BorderPane) loader.load();
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Photos");
			primaryStage.show();
			primaryStage.setResizable(false);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * main - The class that launches the application.
	 * @param args Used to launch the application.
	 */
	public static void main(String[] args) {
        
		launch(args);

	}
}
