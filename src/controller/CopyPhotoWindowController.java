package controller;

import java.io.IOException;
import application.Album;
import application.Photo;
import application.User;
import application.Users;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
/**
 * CopyPhotoWindowController - this window lets the user copy photos from one album to another.
 * @author Alex Smirnov and Raed Anbarserri
 */
public class CopyPhotoWindowController extends Stage {

	@FXML
	Button CreateButton;
	@FXML
	Button CancelButton;
	@FXML
	ListView<String> PossibleAlbumsListView;

	private Users users;
	private User user;
	private Album album;
	private Photo photo;

	private ObservableList<String> obsList;
	
	/**
	 * copy - create button is pressed, copies the current photo into new album.
	 * @param event
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void copy(ActionEvent event) throws IOException, ClassNotFoundException {
		String albumname = PossibleAlbumsListView.getSelectionModel().getSelectedItem();

		if (albumname == null)
			return;

		user.getAlbum(albumname).add(photo);
		Users.store(users);
		
		loadMainWindow(event);

	}
	
	/**
	 * cancel - cancel button is pressed, goes back to main window.
	 * @param event
	 */
	public void cancel(ActionEvent event) {
		loadMainWindow(event); //go back
	}
	
	/**
	 * loadMainWindow - load the window one step before (OpenAlbumWindow).
	 * @param event
	 */
	public void loadMainWindow(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/application/OpenAlbumWindow.fxml"));
			Parent root = (Parent) loader.load();

			OpenAlbumWindowController controller = loader.<OpenAlbumWindowController>getController();
			controller.loadUser(user);
			controller.loadUsers(users);
			controller.loadAlbum(album);
			controller.setup();
			
			Scene scene = new Scene(root);
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * loadUser - load the current user.
	 * @param user
	 */
	public void loadUser(User user) {
		this.user = user;
	}
	/**
	 * loadUsers - load all the users.
	 * @param users
	 */
	public void loadUsers(Users users) {
		this.users = users;
		
	}
	/**
	 * loadAlbum - load the user's album.
	 * @param album
	 */
	public void loadAlbum(Album album) {
		this.album = album;
	}
	/**
	 * loadPhoto - load the current photo.
	 * @param photo
	 */
	public void loadPhoto(Photo photo) {
		this.photo = photo;
		
	}
	/**
	 * view - view the possible albums.
	 */
	public void view() {
		obsList = FXCollections.observableArrayList(user.getAlbumNames());
		PossibleAlbumsListView.setItems(obsList);
	}

}
