package controller;

import java.io.IOException;

import application.Users;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
/**
 * NewUserWindowController - this window lets a user create a new album.
 * @author Alex Smirnov and Raed Anbarserri
 */
public class NewUserWindowController extends Stage {

	@FXML
	Button CreateButton;
	@FXML
	Button CancelButton;
	@FXML
	TextField UsernameTextField;

	Users users;
	
	/**
	 * create - create a new user.
	 * @param event
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void create(ActionEvent event) throws IOException, ClassNotFoundException {

		String username = UsernameTextField.getText();
		username = username.trim();
		if (username.equals("") || username.equals("admin")) {
			return;
		}
		
		users = Users.load();
		users.add(username);
		Users.store(users);
		
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/application/AdminSubsystem.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * cancel - go back to the Admin subsystem.
	 * @param event
	 */
	public void cancel(ActionEvent event) {

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/application/AdminSubsystem.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
