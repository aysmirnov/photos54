package controller;

import java.util.ArrayList;
import application.Album;
import application.Photo;
import application.Tag;
import application.User;
import application.Users;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
/**
 * SearchByTagsWindowController - this window lets a user search for an album by tags.
 * @author Alex Smirnov and Raed Anbarserri
 */
public class SearchByTagsWindowController extends Stage{

	private Users users;
	private User user;
	private Album newAlbum;
	
	private ArrayList<Tag> tags;
	
	@FXML
	TextField TagsTextField;
	
	@FXML
	ListView<String> TagsListView;
	
	private ObservableList<String> obsList;
	/**
	 * search - search for an album by tags.
	 * @param event
	 */
	public void search(ActionEvent event) {
		if (tags.size() == 0) return;
		
		newAlbum = new Album("");
		ArrayList<Album> albums = user.getAlbums();
		for (int i = 0; i < albums.size(); i++) {
			Album album = albums.get(i);
			for (int j = 0; j < album.getSize(); j++) {
				Photo photo = album.getPhoto(j);
				boolean photoAdded = false;
				for (int k = 0; k < photo.returnTags().size(); k++) {
					if (photoAdded) break;
					Tag tag = photo.returnTags().get(k);
					for (int l = 0; l < tags.size(); l++) {
						Tag otherTag = tags.get(l);
						if (tag.getName().toLowerCase().equals(otherTag.getName().toLowerCase()) && tag.getValue().toLowerCase().equals(otherTag.getValue().toLowerCase())){
							newAlbum.add(photo);
							photoAdded = true;
						}
					}
				}

			}
		}
		
		loadSearchWindow(event);
	}
	/**
	 * cancel - go back to the previous window.
	 * @param event
	 */
	public void cancel(ActionEvent event) {
		loadMainWindow(event); //go back
	}
	/**
	 * addTag - add a tag to a photo.
	 * @param event
	 */
	public void addTag(ActionEvent event) {
		String tag = TagsTextField.getText();

		tag = tag.trim();
		
		if (!isValidTag(tag)) return;
		
		String name = tag.substring(0, tag.indexOf(','));
		String value = tag.substring(tag.indexOf(',')+1);
		
		name = name.trim();
		value = value.trim();
		
		tags.add(new Tag(name,value));
		
		updateView();
	}
	/**
	 * removeTag - remove a tag from a photo.
	 * @param event
	 */
	public void removeTag(ActionEvent event) {
		String tag = TagsListView.getSelectionModel().getSelectedItem();
		if (tag == null) return;
		
		String name = tag.substring(0, tag.indexOf(','));
		String value = tag.substring(tag.indexOf(',')+1);
		
		for (int i=0; i < tags.size(); i++) {
			if (tags.get(i).getName().equals(name) && tags.get(i).getValue().equals(value)) {
				tags.remove(i);
			}
		}
		
		updateView();
	}
	/**
	 * isValidTag - check if a tag format is valid.
	 * @param tag
	 * @return
	 */
	private boolean isValidTag(String tag) {
		if (tag.length() == 0) return false;
		
		if (tag.indexOf(',') == -1) return false;

		if (tag.indexOf(',')+1 == tag.length()) return false;
		
		if (tag.substring(tag.indexOf(',')+1).indexOf(',') != -1) return false;
		
		return true;
	}
	/**
	 * updateView - update the listview with new tags.
	 */
	public void updateView() {
		ArrayList<String> tags_to_strings = new ArrayList<String>();
		
		for (int i = 0; i < tags.size(); i++) {
			Tag tag = tags.get(i);
			tags_to_strings.add(tag.toString());
		}
		
		obsList = FXCollections.observableArrayList(tags_to_strings);
		TagsListView.setItems(obsList);
	}
	/**
	 * loadSearchWindow - loads the search results window.
	 * @param event
	 */
	public void loadSearchWindow(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/application/SearchResultsWindow.fxml"));
			Parent root = (Parent) loader.load();

			SearchResultsWindowController controller = loader.<SearchResultsWindowController>getController();
			controller.loadUser(user);
			controller.loadUsers(users);
			controller.loadAlbum(newAlbum);
			controller.setup();
			
			Scene scene = new Scene(root);
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * loadMainWindow - loads the main window.
	 * @param event
	 */
	public void loadMainWindow(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/application/MainApplicationWindow.fxml"));
			Parent root = (Parent) loader.load();
			
			MainApplicationWindowController controller = loader.<MainApplicationWindowController>getController();
			controller.loadUser(user);
			controller.loadUsers(users);
			controller.view();
			
			Scene scene = new Scene(root);
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * initalize - initializes the tags arraylist.
	 */
	public void initialize() {
		tags = new ArrayList<Tag>();
		updateView();
	}
	/**
	 * loadUser - loads the current user.
	 * @param user
	 */
	public void loadUser(User user) {
		this.user = user;
	}
	/**
	 * loadUsers - loads a list of all the users.
	 * @param users
	 */
	public void loadUsers(Users users) {
		this.users = users;
	}
}