package controller;

import java.net.URL;
import java.util.ResourceBundle;

import application.User;
import application.Users;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
/**
 * MainApplicationWindowController - this window lets a user choose an album, delete, rename them, or search for photos.
 * @author Alex Smirnov and Raed Anbarserri
 */
public class MainApplicationWindowController extends Stage implements Initializable {

	Users users;
	User user;
	
	@FXML
	ListView<String> AlbumView;

	@FXML
	TextArea PhotoInfoTextArea;
	
	private ObservableList<String> obsList;
	/**
	 * searchByTags - search for a photo by tags.
	 * @param event
	 */
	public void searchByTags(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/application/SearchByTagsWindow.fxml"));
			Parent root = (Parent) loader.load();
			
			SearchByTagsWindowController controller = loader.<SearchByTagsWindowController>getController();
			controller.loadUser(user);
			controller.loadUsers(users);
			
			Scene scene = new Scene(root);
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * searchByDate - search for a photo by time taken.
	 * @param event
	 */
	public void searchByDate(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/application/SearchByDateWindow.fxml"));
			Parent root = (Parent) loader.load();
			
			SearchByDateWindowController controller = loader.<SearchByDateWindowController>getController();
			controller.loadUser(user);
			controller.loadUsers(users);
			
			Scene scene = new Scene(root);
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * createAlbum - create a new album.
	 * @param event
	 */
	public void createAlbum(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/application/NewAlbumWindow.fxml"));
			Parent root = (Parent) loader.load();
			
			NewAlbumWindowController controller = loader.<NewAlbumWindowController>getController();
			controller.loadUser(user);
			controller.loadUsers(users);
			
			Scene scene = new Scene(root);
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * deleteAlbum - delete the chosen album.
	 */
	public void deleteAlbum() {

		String album = AlbumView.getSelectionModel().getSelectedItem();
		if (album == null) return;
		
		user.deleteAlbum(album);
		Users.store(users);
		view();
	}
	/**
	 * renameAlbumButton - rename the chosen album.
	 * @param event
	 */
	public void renameAlbumButton(ActionEvent event) {
		String album = AlbumView.getSelectionModel().getSelectedItem();

		if (album == null)
			return;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/application/RenameAlbumWindow.fxml"));
			Parent root = (Parent) loader.load();
			
			RenameAlbumWindowController controller = loader.<RenameAlbumWindowController>getController();
			controller.loadUser(user);
			controller.loadUsers(users);
			controller.loadAlbum(user.getAlbum(album));
			
			Scene scene = new Scene(root);
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * openAlbum - open the chosen album.
	 * @param event
	 */
	public void openAlbum(ActionEvent event) {

		String album = AlbumView.getSelectionModel().getSelectedItem();
		if (album == null)
			return;

		try {
			// load ViewAlbumWindow
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/application/OpenAlbumWindow.fxml"));
			Parent root = (Parent) loader.load();
			
			OpenAlbumWindowController controller = loader.<OpenAlbumWindowController>getController();
			controller.loadUser(user);
			controller.loadUsers(users);
			controller.loadAlbum(user.getAlbum(album));
			controller.setup();
			
			Scene scene = new Scene(root);
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * logout - logout from the system, and save work.
	 * @param event
	 */
	public void logout(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/application/LoginWindow.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * initialize - load the albums from last session.
	 * @param arg0
	 * @param arg1
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		PhotoInfoTextArea.setEditable(false);
		AlbumView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
				String albumname = arg2;
				if (albumname == null) {
					PhotoInfoTextArea.setText("");
					return;
				}
				user.getAlbum(albumname).findDates();
				PhotoInfoTextArea.setText("Name: "+user.getAlbum(albumname).getName()+"\n"+
										"Size: "+user.getAlbum(albumname).getSize()+"\n"+
										"Oldest Photo: "+user.getAlbum(albumname).getOldestDate()+"\n"+
										"Newest Photo: "+user.getAlbum(albumname).getNewestDate());
			}
		});
			
		users = Users.load();
	}
	/**
	 * view - view the albums.
	 */
	public void view() {
		obsList = FXCollections.observableArrayList(user.getAlbumNames());
		AlbumView.setItems(obsList);
	}
	/**
	 * loadUser - load the given user.
	 * @param user
	 */
	public void loadUser(User user) {
		this.user = user;
	}
	/**
	 * loadUsers - load all the users.
	 * @param users
	 */
	public void loadUsers(Users users) {
		this.users = users;
		
	}
}
