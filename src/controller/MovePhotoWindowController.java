package controller;

import java.io.IOException;
import application.Album;
import application.Photo;
import application.User;
import application.Users;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
/**
 * MovePhotoWindowController - this window lets a user choose an album, delete, rename them, or search for photos.
 * @author Alex Smirnov and Raed Anbarserri
 */
public class MovePhotoWindowController extends Stage {

	@FXML
	Button CreateButton;
	@FXML
	Button CancelButton;
	@FXML
	ListView<String> PossibleAlbumsListView;

	private Users users;
	private User user;
	private Album album;
	private Photo photo;
	private int index;
	
	private ObservableList<String> obsList;
	
	/**
	 * move - create button is pressed, and photo is moved to a new album.
	 * @param event
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void move(ActionEvent event) throws IOException, ClassNotFoundException {
		String albumname = PossibleAlbumsListView.getSelectionModel().getSelectedItem();

		if (albumname == null)
			return;

		user.getAlbum(albumname).add(photo);
		album.remove(index);
		
		Users.store(users);
		
		loadMainWindow(event);

	}
	
	/**
	 * cancel - goes back to the main window.
	 */
	public void cancel(ActionEvent event) {
		loadMainWindow(event); //go back
	}
	/**
	 * loadMainWindow - loads the main window.
	 * @param event
	 */
	public void loadMainWindow(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/application/OpenAlbumWindow.fxml"));
			Parent root = (Parent) loader.load();

			OpenAlbumWindowController controller = loader.<OpenAlbumWindowController>getController();
			controller.loadUser(user);
			controller.loadUsers(users);
			controller.loadAlbum(album);
			controller.setup();
			
			Scene scene = new Scene(root);
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * loadUser - load the current user.
	 * @param user
	 */
	public void loadUser(User user) {
		this.user = user;
	}
	/**
	 * loadUsers - load all the users.
	 * @param users
	 */
	public void loadUsers(Users users) {
		this.users = users;
		
	}
	/**
	 * loadAlbum - load the user's album.
	 * @param album
	 */
	public void loadAlbum(Album album) {
		this.album = album;
	}
	/**
	 * loadPhoto - load the current photo.
	 * @param photo
	 */
	public void loadPhoto(Photo photo) {
		this.photo = photo;
		
	}	
	/**
	 * loadIndex - loads the index of the photo.
	 * @param index
	 */
	public void loadIndex(int index) {
		this.index = index;
	}
	/**
	 * view - view all album names.
	 */
	public void view() {
		obsList = FXCollections.observableArrayList(user.getAlbumNames());
		PossibleAlbumsListView.setItems(obsList);
	}

}
