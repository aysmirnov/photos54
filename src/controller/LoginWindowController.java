package controller;

import java.net.URL;
import java.util.ResourceBundle;

import application.User;
import application.Users;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
/**
 * LoginWindowController - this window lets a user or the admin log in.
 * @author Alex Smirnov and Raed Anbarserri
 */
public class LoginWindowController extends Stage implements Initializable{

	@FXML
	Button LoginButton;
	@FXML
	Button QuitButton;
	@FXML
	TextField UsernameTextField;
	
	private Users users;
	private User user;
	
	/**
	 * login - login button is pressed, admin goes to admin subsystem, user goes to main window.
	 * @param event
	 */
	public void login(ActionEvent event) {
		String username = UsernameTextField.getText();
		if (username.equals("admin")) {
			admin(event);
		}
		else if (username.equals("")) {
			System.out.println("Please enter a username");
		}
		else {
			user = users.getUser(username);
			
			if (user == null) {
				System.out.println("User doesn't exist");
				return;
			}
			
			try {
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource("/application/MainApplicationWindow.fxml"));
				Parent root = (Parent) loader.load();
				
				MainApplicationWindowController controller = loader.<MainApplicationWindowController>getController();
				controller.loadUser(user);
				controller.loadUsers(users);
				controller.view();
				
				Scene scene = new Scene(root);
				Stage primaryStage = (Stage) ((Node)event.getSource()).getScene().getWindow();
				primaryStage.setScene(scene);
				primaryStage.show();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * admin - admin login.
	 * @param event
	 */
	public void admin(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/application/AdminSubsystem.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			Stage primaryStage = (Stage) ((Node)event.getSource()).getScene().getWindow();
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * initialize - load the users from last session.
	 * @param arg0
	 * @param arg1
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		users = Users.load();
	}
}
