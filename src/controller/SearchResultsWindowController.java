package controller;

import java.io.File;
import java.util.ArrayList;
import application.Album;
import application.User;
import application.Users;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
/**
 * SearchResuyltsWindowController - this window displays the results of a search, and lets the user manipuate them.
 * @author Alex Smirnov and Raed Anbarserri
 */
public class SearchResultsWindowController extends Stage{

	private Users users;
	private User user;
	private Album album;
	private int index,size;
	
	@FXML
	ImageView PhotoView;

	@FXML
	TextArea CaptionTextArea;
	
	@FXML
	ListView<String> TagsListView;
	
	@FXML
	TextField NewAlbumNameTextField;
	
	private ObservableList<String> obsList;
	/**
	 * add - add a photo to an album.
	 * @param event
	 */
	public void add(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		
		File file = fileChooser.showOpenDialog(primaryStage);
		if (file == null) return; //no file selected
		Image image = new Image(file.toURI().toString());
		try {
			image.getHeight();
		} catch (Exception e) {
			System.out.println("Wrong type of file");
			return;
		}
		album.add(image);
		update(); //updates values of album
		
		//change view to new image
		updateLatestView();

	}
	/**
	 * remove - removes a photo.
	 */
	public void remove() {
		if (index < 0) return;
		album.remove(index); //removes photo
		update();
		updateLatestView();
	}
	/**
	 * createNewAlbum - creates a new album of the search results.
	 * @param event
	 */
	public void createNewAlbum(ActionEvent event) {
		String albumname = NewAlbumNameTextField.getText();
		if (albumname.equals("") || user.albumExists(albumname)) {
			System.out.println("please enter an album name");
			return;
		}
		
		album.setName(albumname);
		user.addAlbum(album);
		Users.store(users);
		loadMainWindow(event);
	}
	/**
	 * left - goes left 1 step.
	 */
	public void left() {
		if (index == 0) index = size-1;
		else index--;
		updateLatestView();
	}
	/**
	 * right - goes right 1 step.
	 */
	public void right() {
		if (index+1 == size) index = 0;
		else index++;
		updateLatestView();
	}
	/**
	 * back - goes back to the open album window.
	 * @param event
	 */
	public void back(ActionEvent event) {
		loadMainWindow(event);
	}
	/**
	 * update - updates album size.
	 */
	private void update() {
		size = album.getSize();
		index = size-1;
	}
	/**
	 * setup - setup the album.
	 */
	public void setup() {
		size = album.getPhotos().size();
		
		index = album.getPhotos().size()-1;
		CaptionTextArea.setEditable(false);
		updateLatestView();
	}
	/**
	 * updateLatestView - update the latest view of the photo display.
	 */
	public void updateLatestView() {
		
		if (index < 0) {
			PhotoView.setImage(null);
			CaptionTextArea.setText(album.getName()+ "\n" + "(no images to display)");
			tagsView();
			return;
		}
		
		PhotoView.setImage(album.getImage(index));
		
		String photoInfo = "";
		photoInfo += album.getName() + "\n";
		photoInfo += (index+1)+" out of "+size + "\n";
		photoInfo += album.getPhoto(index).getDate();
		photoInfo += "\n";
		photoInfo += album.getPhoto(index).getCaption();
		
		CaptionTextArea.setText(photoInfo);
		tagsView();
	}
	/**
	 * tagsView - view the tags on a photo.
	 */
	public void tagsView() {
		if (index < 0) {
			TagsListView.setItems(FXCollections.observableArrayList(new ArrayList<String>()));
			return;
		}
		obsList = FXCollections.observableArrayList(album.getPhoto(index).getTags());
		TagsListView.setItems(obsList);
	}
	/**
	 * loadMainWindow - loads the main window of the application.
	 * @param event
	 */
	public void loadMainWindow(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/application/MainApplicationWindow.fxml"));
			Parent root = (Parent) loader.load();
			
			MainApplicationWindowController controller = loader.<MainApplicationWindowController>getController();
			controller.loadUser(user);
			controller.loadUsers(users);
			controller.view();
			
			Scene scene = new Scene(root);
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * loadUser - loads the current user.
	 * @param user
	 */
	public void loadUser(User user) {
		this.user = user;
	}
	/**
	 * loadUsers - loads a list of all the users.
	 * @param users
	 */
	public void loadUsers(Users users) {
		this.users = users;
	}
	/**
	 * loadAlbum - loads the current album.
	 * @param album
	 */
	public void loadAlbum(Album album) {
		this.album = album;
	}
}
