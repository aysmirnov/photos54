package controller;

import java.io.IOException;
import application.User;
import application.Users;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
/**
 * NewAlbumWindowController - this window lets a user create a new album.
 * @author Alex Smirnov and Raed Anbarserri
 */
public class NewAlbumWindowController extends Stage {

	@FXML
	Button CreateButton;
	@FXML
	Button CancelButton;
	@FXML
	TextField UsernameTextField;

	private Users users;
	private User user;

	/**
	 * create - creates a new album with the given name.
	 * @param event
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void create(ActionEvent event) throws IOException, ClassNotFoundException {

		String albumname = UsernameTextField.getText();
		albumname = albumname.trim();
		if (albumname.equals("") || user.albumExists(albumname)) {
			return;
		}
		albumname = albumname.trim();
		//users = Users.load();
		user.addAlbum(albumname);
		Users.store(users);
		
		loadMainWindow(event);

	}
	
	/**
	 * cancel - goes back to the main window.
	 * @param event
	 */
	public void cancel(ActionEvent event) {
		loadMainWindow(event); //go back
	}
	
	/**
	 * loadMainWindow - loads the main window of the application.
	 * @param event
	 */
	public void loadMainWindow(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/application/MainApplicationWindow.fxml"));
			Parent root = (Parent) loader.load();

			MainApplicationWindowController controller = loader.<MainApplicationWindowController>getController();
			controller.loadUser(user);
			controller.loadUsers(users);
			controller.view();
			
			Scene scene = new Scene(root);
			Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * loadUser - load the current user.
	 * @param user
	 */
	public void loadUser(User user) {
		this.user = user;
	}
	/**
	 * loadUsers - load all the users.
	 * @param users
	 */
	public void loadUsers(Users users) {
		this.users = users;
	}

}
